<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220117181721 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP INDEX IDX_EE1C1A7925E254A1');
        $this->addSql('DROP INDEX IDX_EE1C1A796BBD148');
        $this->addSql('CREATE TEMPORARY TABLE __temp__musique_playlist AS SELECT musique_id, playlist_id FROM musique_playlist');
        $this->addSql('DROP TABLE musique_playlist');
        $this->addSql('CREATE TABLE musique_playlist (musique_id INTEGER NOT NULL, playlist_id INTEGER NOT NULL, PRIMARY KEY(musique_id, playlist_id), CONSTRAINT FK_EE1C1A7925E254A1 FOREIGN KEY (musique_id) REFERENCES musique (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE, CONSTRAINT FK_EE1C1A796BBD148 FOREIGN KEY (playlist_id) REFERENCES playlist (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('INSERT INTO musique_playlist (musique_id, playlist_id) SELECT musique_id, playlist_id FROM __temp__musique_playlist');
        $this->addSql('DROP TABLE __temp__musique_playlist');
        $this->addSql('CREATE INDEX IDX_EE1C1A7925E254A1 ON musique_playlist (musique_id)');
        $this->addSql('CREATE INDEX IDX_EE1C1A796BBD148 ON musique_playlist (playlist_id)');
        $this->addSql('CREATE TEMPORARY TABLE __temp__playlist AS SELECT id, nom, image FROM playlist');
        $this->addSql('DROP TABLE playlist');
        $this->addSql('CREATE TABLE playlist (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, creator_id INTEGER DEFAULT NULL, nom VARCHAR(255) NOT NULL COLLATE BINARY, image VARCHAR(510) NOT NULL, CONSTRAINT FK_D782112D61220EA6 FOREIGN KEY (creator_id) REFERENCES user (id) NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('INSERT INTO playlist (id, nom, image) SELECT id, nom, image FROM __temp__playlist');
        $this->addSql('DROP TABLE __temp__playlist');
        $this->addSql('CREATE INDEX IDX_D782112D61220EA6 ON playlist (creator_id)');
        $this->addSql('DROP INDEX UNIQ_8D93D6496C6E55B5');
        $this->addSql('CREATE TEMPORARY TABLE __temp__user AS SELECT id, nom, roles, password, picture FROM user');
        $this->addSql('DROP TABLE user');
        $this->addSql('CREATE TABLE user (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, nom VARCHAR(180) NOT NULL COLLATE BINARY, roles CLOB NOT NULL --(DC2Type:json)
        , password VARCHAR(255) NOT NULL COLLATE BINARY, picture VARCHAR(1024) NOT NULL COLLATE BINARY)');
        $this->addSql('INSERT INTO user (id, nom, roles, password, picture) SELECT id, nom, roles, password, picture FROM __temp__user');
        $this->addSql('DROP TABLE __temp__user');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_8D93D6496C6E55B5 ON user (nom)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP INDEX IDX_EE1C1A7925E254A1');
        $this->addSql('DROP INDEX IDX_EE1C1A796BBD148');
        $this->addSql('CREATE TEMPORARY TABLE __temp__musique_playlist AS SELECT musique_id, playlist_id FROM musique_playlist');
        $this->addSql('DROP TABLE musique_playlist');
        $this->addSql('CREATE TABLE musique_playlist (musique_id INTEGER NOT NULL, playlist_id INTEGER NOT NULL, PRIMARY KEY(musique_id, playlist_id))');
        $this->addSql('INSERT INTO musique_playlist (musique_id, playlist_id) SELECT musique_id, playlist_id FROM __temp__musique_playlist');
        $this->addSql('DROP TABLE __temp__musique_playlist');
        $this->addSql('CREATE INDEX IDX_EE1C1A7925E254A1 ON musique_playlist (musique_id)');
        $this->addSql('CREATE INDEX IDX_EE1C1A796BBD148 ON musique_playlist (playlist_id)');
        $this->addSql('DROP INDEX IDX_D782112D61220EA6');
        $this->addSql('CREATE TEMPORARY TABLE __temp__playlist AS SELECT id, image, nom FROM playlist');
        $this->addSql('DROP TABLE playlist');
        $this->addSql('CREATE TABLE playlist (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, image VARCHAR(510) DEFAULT NULL COLLATE BINARY, nom VARCHAR(255) NOT NULL)');
        $this->addSql('INSERT INTO playlist (id, image, nom) SELECT id, image, nom FROM __temp__playlist');
        $this->addSql('DROP TABLE __temp__playlist');
        $this->addSql('DROP INDEX UNIQ_8D93D6496C6E55B5');
        $this->addSql('CREATE TEMPORARY TABLE __temp__user AS SELECT id, nom, roles, password, picture FROM user');
        $this->addSql('DROP TABLE user');
        $this->addSql('CREATE TABLE user (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, nom VARCHAR(180) NOT NULL, roles CLOB NOT NULL COLLATE BINARY --(DC2Type:json)
        , password VARCHAR(255) NOT NULL, picture VARCHAR(1024) NOT NULL)');
        $this->addSql('INSERT INTO user (id, nom, roles, password, picture) SELECT id, nom, roles, password, picture FROM __temp__user');
        $this->addSql('DROP TABLE __temp__user');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_8D93D6496C6E55B5 ON user (nom)');
    }
}
