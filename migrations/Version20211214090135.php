<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211214090135 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP INDEX IDX_EE1C1A796BBD148');
        $this->addSql('DROP INDEX IDX_EE1C1A7925E254A1');
        $this->addSql('CREATE TEMPORARY TABLE __temp__musique_playlist AS SELECT musique_id, playlist_id FROM musique_playlist');
        $this->addSql('DROP TABLE musique_playlist');
        $this->addSql('CREATE TABLE musique_playlist (musique_id INTEGER NOT NULL, playlist_id INTEGER NOT NULL, PRIMARY KEY(musique_id, playlist_id), CONSTRAINT FK_EE1C1A7925E254A1 FOREIGN KEY (musique_id) REFERENCES musique (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE, CONSTRAINT FK_EE1C1A796BBD148 FOREIGN KEY (playlist_id) REFERENCES playlist (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('INSERT INTO musique_playlist (musique_id, playlist_id) SELECT musique_id, playlist_id FROM __temp__musique_playlist');
        $this->addSql('DROP TABLE __temp__musique_playlist');
        $this->addSql('CREATE INDEX IDX_EE1C1A796BBD148 ON musique_playlist (playlist_id)');
        $this->addSql('CREATE INDEX IDX_EE1C1A7925E254A1 ON musique_playlist (musique_id)');
        $this->addSql('ALTER TABLE playlist ADD COLUMN image VARCHAR(510)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP INDEX IDX_EE1C1A7925E254A1');
        $this->addSql('DROP INDEX IDX_EE1C1A796BBD148');
        $this->addSql('CREATE TEMPORARY TABLE __temp__musique_playlist AS SELECT musique_id, playlist_id FROM musique_playlist');
        $this->addSql('DROP TABLE musique_playlist');
        $this->addSql('CREATE TABLE musique_playlist (musique_id INTEGER NOT NULL, playlist_id INTEGER NOT NULL, PRIMARY KEY(musique_id, playlist_id))');
        $this->addSql('INSERT INTO musique_playlist (musique_id, playlist_id) SELECT musique_id, playlist_id FROM __temp__musique_playlist');
        $this->addSql('DROP TABLE __temp__musique_playlist');
        $this->addSql('CREATE INDEX IDX_EE1C1A7925E254A1 ON musique_playlist (musique_id)');
        $this->addSql('CREATE INDEX IDX_EE1C1A796BBD148 ON musique_playlist (playlist_id)');
        $this->addSql('CREATE TEMPORARY TABLE __temp__playlist AS SELECT id, nom FROM playlist');
        $this->addSql('DROP TABLE playlist');
        $this->addSql('CREATE TABLE playlist (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, nom VARCHAR(255) NOT NULL)');
        $this->addSql('INSERT INTO playlist (id, nom) SELECT id, nom FROM __temp__playlist');
        $this->addSql('DROP TABLE __temp__playlist');
    }
}
