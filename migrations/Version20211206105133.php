<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211206105133 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE musique (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, titre VARCHAR(255) NOT NULL, artiste VARCHAR(255) NOT NULL, image VARCHAR(510) NOT NULL, extrait VARCHAR(510) NOT NULL)');
        $this->addSql('CREATE TABLE musique_playlist (musique_id INTEGER NOT NULL, playlist_id INTEGER NOT NULL, PRIMARY KEY(musique_id, playlist_id))');
        $this->addSql('CREATE INDEX IDX_EE1C1A7925E254A1 ON musique_playlist (musique_id)');
        $this->addSql('CREATE INDEX IDX_EE1C1A796BBD148 ON musique_playlist (playlist_id)');
        $this->addSql('CREATE TABLE playlist (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, nom VARCHAR(255) NOT NULL)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE musique');
        $this->addSql('DROP TABLE musique_playlist');
        $this->addSql('DROP TABLE playlist');
    }
}
