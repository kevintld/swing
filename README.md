# Projet Swing

Le projet ```Swing``` a pour but de mettre à disposition une liste de musiques regroupées ou non dans des playlists. Chaque musique possède une page de détails, peut être jouée et entièrement customisable à même titre que les playlists. Egalement, trois status d'utilisateurs sont distinguables :
- les visiteurs, qui peuvent simplement écouter les playlists
- les administrateurs, qui peuvent ajouter/modifier/supprimer les playlists et musiques et gérer les visiteurs
- les super-administrateurs, qui peuvent tout gérer et nommer des administrateurs
```Swing``` est réalisé dans le cadre d'un projet informatique à l'[IUT Informatique d'Orléans](https://www.univ-orleans.fr/fr/iut-orleans) et met en application une API REST basée sur Symfony 5, SvelteJS et SQLite et entièrement déployable via Docker.

## Contributeurs

- [DEVOS Nicolas](https://gitlab.com/nicotoine)
- [PLOUY-ROSSARD Elie](https://gitlab.com/elieplr)
- [TALLAND Kevin](https://gitlab.com/kevintld)

## Installation

### > Pré-requis

[Composer](https://getcomposer.org/), [Symfony](https://symfony.com/), [NodeJS](https://nodejs.org/en/) et/ou [Docker](https://www.docker.com/) sont requis pour lancer ce projet. 

### Installation manuelle

1. Dans un premier temps, placez-vous dans votre dossier favori prêt à accueillir l'application et récupérez le projet [swing](https://gitlab.com/kevintld/swing) via la commande :
```bash
git clone https://gitlab.com/kevintld/swing
```

2. Ensuite, installez les dépendances du projet :
```bash
cd swing
composer install
npm i
npm run dev
```

3. Maintenant, il faut gérer la base de données. Nous allons donc la créer puis effectuer les modifications nécessaires sur celle-ci :
- Commentez la ligne suivante dans le fichier .env
```bash
# DATABASE_URL="mysql://swing:swing@swing_db/swing?serverVersion=5.7"
```
- Décommentez la ligne suivante dans le fichier .env
```bash
DATABASE_URL="sqlite:///%kernel.project_dir%/var/data.db"
```
- Effectuer les modifications
```bash
symfony console doctrine:database:create
symfony console doctrine:migrations:migrate --no-interaction
```

4. Lançons maintenant le serveur symfony :
```bash
symfony serve -d
```

Le site [Swing](http://localhost:8000) est désormais accessible !

### Installation via docker

1. Identifique à l'installation manuelle
2. Décommentez la ligne suivante dans le fichier .env
```bash
DATABASE_URL="mysql://swing:swing@swing_db/swing?serverVersion=5.7"
```
3. Commentez la ligne suivante dans le fichier .env
```bash
# DATABASE_URL="sqlite:///%kernel.project_dir%/var/data.db"
```
4. Lançons nos containers. Cela peut durer 5 minutes.
```bash
cd swing
docker-compose up -d
```
5. Attendez 1 minute supplémentaire.

Le site [Swing](http://localhost:8000) est désormais accessible !

## Fonctionnalités

### > Scraping
Le scrapping permet de récuperer des musiques et playlists depuis le site [ats-studio](https://www.ats-studios.com/).

Pour l'utiliser il suffit de se placer à la racine du projet et d'exécuter la commande.

```
node ./music_scrapping/cheerio.js
```
Ensuite rendez-vous simplement dans la partie administrative et actualiser les musiques via le bouton en haut à droite.

### > Globales

Depuis notre site il est possible :
- d'écouter une musique ou des playlists (```visiteur```)
- d'ajouter, de modifier, de supprimer une playlist ou une musique (```administrateur et utilisateur```),
- de gérer les utilisateurs (```administrateur```),
- de gérer les administrateurs (```super-administrateur```),

Enfin, notre site est responsive.