<?php

namespace App\Controller;

use App\Entity\Musique;
use App\Entity\Playlist;
use App\Entity\User;
use App\Repository\MusiqueRepository;
use App\Repository\PlaylistRepository;
use Symfony\Component\Security\Core\Authentication\AuthenticationManagerInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/api/playlist/v1.0")
 */
class PlaylistController extends AbstractController
{
    private TokenStorageInterface $token;
    private AuthenticationManagerInterface $authenticationManager;

    public function __construct(AuthenticationManagerInterface $authenticationManager, TokenStorageInterface $token)
    {
        $this->token = $token;
        $this->authenticationManager = $authenticationManager;
    }

    /**
     * @Route("/liste", name="listeAll_playlist")
     */
    public function listeAll(PlaylistRepository $playlistRepository): Response
    {
        $listePlaylist = $playlistRepository->findAll();

        $serializer = $this->container->get('serializer');
        $playlists = $serializer->serialize($listePlaylist, 'json', [
            'circular_reference_handler' => function ($object) {
                return $object->getId();
            }   
        ]);
        $reponse = new Response();
        $reponse->setContent($playlists);
        $reponse->headers->set("Content-Type", "application/json");
        $reponse->headers->set("Access-Control-Allow-Origin", "*");
        
        return $reponse;
    }
    /**
     * @Route("/liste/page/{numPage<\d+>}", name="liste_playlist")
     */
    public function liste(PlaylistRepository $playlistRepository,int $numPage): Response
    {
        $listePlaylist = $playlistRepository->findBy(array(),array(),10,10*($numPage));

        $serializer = $this->container->get('serializer');
        $playlists = $serializer->serialize($listePlaylist, 'json', [
            'circular_reference_handler' => function ($object) {
                return $object->getId();
            }   
        ]);
        $reponse = new Response();
        $reponse->setContent($playlists);
        $reponse->headers->set("Content-Type", "application/json");
        $reponse->headers->set("Access-Control-Allow-Origin", "*");
        
        return $reponse;
    }
    /**
     * @Route("/size", name="numbers_playlist")
     */
    public function size(PlaylistRepository $playlistRepository): Response
    {
        $listePlaylist =$playlistRepository->findAll();
        $data = count($listePlaylist);
        
        $reponse = new Response();
        $reponse->setContent(json_encode(array("taille"=>$data)));
        $reponse->headers->set("Content-Type", "application/json");
        $reponse->headers->set("Access-Control-Allow-Origin", "*");

        return $reponse;
    }
    /**
     * @Route("/ajout", name="ajout_playlist")
     */
    public function ajout(MusiqueRepository $musiqueRepository,Request $request) : Response
    {
        if (
            !$this->token->getToken() ||
            !in_array("utilisateur", $this->token->getToken()->getUser()->getRoles())
        ) return $this->redirectToRoute("app");

        // On récupère les données
        $body = json_decode($request->getContent(), true);
        $nom = $body["nom"];
        $musiques = $body["musiques"];
        $image = $body["img"];
        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository(User::class)->find($body["user"]);
        
        if(strlen($image)<1)$image="https://www.ats-studios.com/POCHETTES/thumbnail/6087.340_72.png";
            
        $playlist = new Playlist();
        // On modifie la playlist
        $playlist->setNom($nom);
        $playlist->setImage($image);
        $playlist->setCreator($user);
     
        foreach($musiques as $id){
            $musique= $musiqueRepository->find($id);
            $playlist->addMusique($musique);
        }
        
        // On stocke la playlist
        $em->persist($playlist);
        $em->flush();

        $serializer = $this->container->get('serializer');
        $playlist_ret = $serializer->serialize($playlist, 'json', [
            'circular_reference_handler' => function ($object) {
                return $object->getId();
            }   
        ]);
        $reponse = new Response();
        $reponse->setContent($playlist_ret);
        $reponse->headers->set("Content-Type", "application/json");
        $reponse->headers->set("Access-Control-Allow-Origin", "*");
        return $reponse;
    }

    /**
     * @Route("/liste/{id<\d+>}", name="details_playlist_by_id")
     */
    public function details_by_id(int $id, PlaylistRepository $playlistRepository) : Response
    {
       
        $serializer = $this->container->get('serializer');
        $playlist = $serializer->serialize($playlistRepository->find($id), 'json', [
            'circular_reference_handler' => function ($object) {
                return $object->getId();
            }   
        ]);
        $reponse = new Response();
        $reponse->setContent($playlist);
        $reponse->headers->set("Content-Type", "application/json");
        $reponse->headers->set("Access-Control-Allow-Origin", "*");
        
        return $reponse;
    }

    /**
     * @Route("/edition/{id}", name="edition_playlist")
     */
    public function edition(Request $request, PlaylistRepository $playlistRepository, int $id,MusiqueRepository $musiqueRepository) : Response
    {
        $playlist = $playlistRepository->find($id);

        if (
            !$this->token->getToken() ||
            !in_array("utilisateur", $this->token->getToken()->getUser()->getRoles())
        ) return $this->redirectToRoute("app");

        $body = json_decode($request->getContent(), true);
        // On récupère les données
        $nom = $body["nom"] ?? $playlist->getNom();
        $musiques = $body["musiques"];
        $image = $body["img"] ;
        
        if(strlen($image)<1)$image="https://www.ats-studios.com/POCHETTES/thumbnail/6087.340_72.png";
       
        // On modifie la playlist
        $playlist->setNom($nom);
        $playlist->setImage($image);

        if(count($musiques)>0)
        {
            foreach($playlist->getMusiques() as $musique){
                $playlist->removeMusique($musique);
            }
            foreach($musiques as $id){
                $musique= $musiqueRepository->find($id);
                $playlist->addMusique($musique);
            }
        }           
       
        // On stocke la playlist
        $em = $this->getDoctrine()->getManager();
        $em->persist($playlist);
        $em->flush();

        $serializer = $this->container->get('serializer');
        $playlist_ret = $serializer->serialize($playlist, 'json', [
            'circular_reference_handler' => function ($object) {
                return $object->getId();
            }   
        ]);
        $reponse = new Response();
        $reponse->setContent($playlist_ret);
        $reponse->headers->set("Content-Type", "application/json");
        $reponse->headers->set("Access-Control-Allow-Origin", "*");
        return $reponse;
    }
    
    /**
     * @Route("/delete/{id}", name="delete_playlist")
     */
    public function delete( PlaylistRepository $playlistRepository, int $id) : Response
    {
        $playlist = $playlistRepository->find($id);

        if (
            !$this->token->getToken() ||
            !in_array("utilisateur", $this->token->getToken()->getUser()->getRoles())
        ) return $this->redirectToRoute("app");

        // On récupère les données
        $succes = $playlist !== null ;
        // On renvoie la musique fraichement créé
        $reponse = new Response(json_encode(array
        (
            "succes" => $succes
        )
        ));
        $reponse->headers->set("Content-Type", "application/json");
        $reponse->headers->set("Access-Control-Allow-Origin", "*");

        if($succes){
            $em = $this->getDoctrine()->getManager();
            $em->remove($playlist);
            $em->flush();
        }
       
        return $reponse;
    }

}


