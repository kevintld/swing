<?php

namespace App\Controller;

use App\Entity\Musique;
use App\Repository\MusiqueRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Core\Authentication\AuthenticationManagerInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/api/musique/v1.0")
 */
class MusiqueController extends AbstractController
{
    private TokenStorageInterface $token;
    private AuthenticationManagerInterface $authenticationManager;

    public function __construct(AuthenticationManagerInterface $authenticationManager, TokenStorageInterface $token)
    {
        $this->token = $token;
        $this->authenticationManager = $authenticationManager;
    }

    /**
     * @Route("/liste/page/{numPage<\d+>}", name="liste_musique")
     */
    public function liste(MusiqueRepository $musiqueRepository,int $numPage): Response
    {
        // $musiqueRepository->findAll();
        $listeMusique =$musiqueRepository->findBy(array(),array(),20,20*($numPage));
        $data= array();

        foreach($listeMusique as $musique){
            array_push($data, array(
                "id" =>         $musique->getId(),
                "titre"=>       $musique->getTitre(),
                "artiste"=>     $musique->getArtiste(),
                "image"=>       $musique->getImage(),
                "extrait"=>     $musique->getExtrait(),
                "playlists"=>   $musique->getPlaylists()
            ));
        }
        $reponse = new Response();
        $reponse->setContent(json_encode(array("musiques"=>$data)));
        $reponse->headers->set("Content-Type", "application/json");
        $reponse->headers->set("Access-Control-Allow-Origin", "*");
        
        return $reponse;
    }
     /**
     * @Route("/liste", name="listeAll_musique")
     */
    public function listeAll(MusiqueRepository $musiqueRepository): Response
    {
        $listeMusique =$musiqueRepository->findAll();
        $data= array();

        foreach($listeMusique as $musique){
            array_push($data, array(
                "id" =>         $musique->getId(),
                "titre"=>       $musique->getTitre(),
                "artiste"=>     $musique->getArtiste(),
                "image"=>       $musique->getImage(),
                "extrait"=>     $musique->getExtrait(),
                "playlists"=>   $musique->getPlaylists()
            ));
        }
        $reponse = new Response();
        $reponse->setContent(json_encode(array("musiques"=>$data)));
        $reponse->headers->set("Content-Type", "application/json");
        $reponse->headers->set("Access-Control-Allow-Origin", "*");
        
        return $reponse;
    }
    /**
     * @Route("/size", name="numbers_musique")
     */
    public function size(MusiqueRepository $musiqueRepository): Response
    {
        $listeMusique = $musiqueRepository->findAll();
        $data = count($listeMusique);
        
        $reponse = new Response();
        $reponse->setContent(json_encode(array("taille"=>$data)));
        $reponse->headers->set("Content-Type", "application/json");
        $reponse->headers->set("Access-Control-Allow-Origin", "*");

        return $reponse;
    }
    /**
     * @Route("/ajout", name="ajout_musique")
     */
    public function ajout(Request $request) : Response
    {
        if (
            !$this->token->getToken() ||
            !in_array("administrateur", $this->token->getToken()->getUser()->getRoles())
        ) return $this->redirectToRoute("app");

        // On récupère les données
        $body = json_decode($request->getContent(), true);
        $titre = $body["titre"] ?? "";
        $artiste = $body["artiste"] ?? "";
        $img = $body["img"] ?? "";
        $extrait = $body["extrait"] ?? "";
        
        // On créé une musique
        $musique = new Musique();
        $musique->setTitre($titre);
        $musique->setArtiste($artiste);
        $musique->setImage($img);
        $musique->setExtrait($extrait);

        // On stocke la musique
        $em = $this->getDoctrine()->getManager();
        $em->persist($musique);
        $em->flush();

        // On renvoie la musique fraichement créé
        $reponse = new Response(json_encode(array
        (
            "id" =>         $musique->getId(),
            "titre"=>       $musique->getTitre()
        )
        ));

        $reponse->headers->set("Content-Type", "application/json");
        $reponse->headers->set("Access-Control-Allow-Origin", "*");
        return $reponse;
    }

    /**
     * @Route("/liste/{id<\d+>}", name="details_musique_by_id")
     */
    public function details_by_id(int $id, MusiqueRepository $musiqueRepository) : Response
    {
        $musique = $musiqueRepository->find($id);

        // On renvoie la musique demandéé
        $reponse = new Response(json_encode(array
        (
            "id" =>         $musique->getId(),
            "titre"=>       $musique->getTitre(),
            "artiste"=>     $musique->getArtiste(),
            "image"=>       $musique->getImage(),
            "extrait"=>     $musique->getExtrait(),
            "playlists"=>   $musique->getPlaylists()
        )
        ));

        $reponse->headers->set("Content-Type", "application/json");
        $reponse->headers->set("Access-Control-Allow-Origin", "*");
        return $reponse;
    }

    /**
     * @Route("/edition/{id}", name="edition_musique")
     */
    public function edition(Request $request, MusiqueRepository $musiqueRepository, int $id) : Response
    {
        if (
            !$this->token->getToken() ||
            !in_array("administrateur", $this->token->getToken()->getUser()->getRoles())
        ) return $this->redirectToRoute("app");

        $body = json_decode($request->getContent(), true);
        // On récupère les données
        $musique = $musiqueRepository->find($id);
        $titre = $body["titre"] ?? $musique->getTitre();
        $artiste = $body["artiste"] ?? $musique->getArtiste();
        $img = $body["img"] ?? $musique->getImage();
        $extrait = $body["extrait"] ?? $musique->getExtrait();
        $playlists = $body["playlists"] ?? $musique->getPlaylists();
        
        // On modifie la musique
        $musique->setTitre($titre);
        $musique->setArtiste($artiste);
        $musique->setImage($img);
        $musique->setExtrait($extrait);
        foreach($playlists as $playlist){
            $musique->addPlaylist($playlist);
        }

        // On stocke la musique
        $em = $this->getDoctrine()->getManager();
        $em->persist($musique);
        $em->flush();

        // On renvoie la musique fraichement créé
        $reponse = new Response(json_encode(array
        (
            "id" =>         $musique->getId(),
            "titre"=>       $musique->getTitre()
        )
        ));

        $reponse->headers->set("Content-Type", "application/json");
        $reponse->headers->set("Access-Control-Allow-Origin", "*");
        return $reponse;
    }
    
    /**
     * @Route("/delete/{id}", name="delete_musique")
     */
    public function delete( MusiqueRepository $musiqueRepository, int $id) : Response
    {
        if (
            !$this->token->getToken() ||
            !in_array("administrateur", $this->token->getToken()->getUser()->getRoles())
        ) return $this->redirectToRoute("app");

        // On récupère les données
        $musique = $musiqueRepository->find($id);
        
        $succes = $musique !== null ;
        // On renvoie la musique fraichement créé
        $reponse = new Response(json_encode(array
        (
            "succes" => $succes
        )
        ));
        $reponse->headers->set("Content-Type", "application/json");
        $reponse->headers->set("Access-Control-Allow-Origin", "*");

        if($succes){
            $em = $this->getDoctrine()->getManager();
            $em->remove($musique);
            $em->flush();
        }
       
        return $reponse;
    }

}
