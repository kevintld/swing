<?php

namespace App\Controller;

use App\Entity\User;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Authentication\AuthenticationManagerInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;

/**
 * @Route("/api/user/v1.0")
 */
class UserController extends AbstractController
{
    private TokenStorageInterface $token;
    private AuthenticationManagerInterface $authenticationManager;

    public function __construct(AuthenticationManagerInterface $authenticationManager, TokenStorageInterface $token)
    {
        $this->token = $token;
        $this->authenticationManager = $authenticationManager;
    }
    
    /**
     * @Route("/liste", name="liste_user")
     */
    public function liste(UserRepository $userRepository): Response
    {
        if (
            !$this->token->getToken() ||
            !in_array("administrateur", $this->token->getToken()->getUser()->getRoles())
        ) return $this->redirectToRoute("app");
        
        $listeUser = $userRepository->findAll();
        $data = array();

        foreach ($listeUser as $user)
        {
            array_push($data, array(
                "id" => $user->getId(),
                "nom" => $user->getNom(),
                "roles" => $user->getRoles(),
                "img" => $user->getPicture(),
            ));
        }

        $reponse = new Response();
        $reponse->setContent(json_encode(array("users"=>$data)));
        $reponse->headers->set("Content-Type", "application/json");
        $reponse->headers->set("Access-Control-Allow-Origin", "*");
        
        return $reponse;
    }

     /**
     * @Route("/liste/{id<\d+>}", name="details_user")
     */
    public function details(UserRepository $userRepository, int $id): Response
    {
        if (
            !$this->token->getToken() ||
            !in_array("utilisateur", $this->token->getToken()->getUser()->getRoles()) ||
            (
                !in_array("administrateur", $this->token->getToken()->getUser()->getRoles()) &&
                $this->token->getToken()->getUser()->getId() != $id
            )
        ) return $this->redirectToRoute("app");
        
        $user = $userRepository->find($id);
        if (!$user)
            return new Response("Aucun utilisateur trouvé");

        $reponse = new Response();
        $reponse->setContent(json_encode(array(
            "id" => $user->getId(),
            "nom" => $user->getUserIdentifier(),
            "img" => $user->getPicture(),
            "roles" => $user->getRoles(),
        )));
        $reponse->headers->set("Content-Type", "application/json");
        $reponse->headers->set("Access-Control-Allow-Origin", "*");
        
        return $reponse;
    }

    /**
     * @Route("/ajout", name="ajout_user")
     */
    public function ajout(Request $request, UserPasswordHasherInterface $encoder) : Response
    {
        // On récupère les données
        $body = json_decode($request->getContent(), true);
        $nom = $body["nom"];
        $password = $body["pwd"];
        $img = $body["img"] ?? "https://eu.ui-avatars.com/api/?name=$nom";

        $repo = $this->getDoctrine()->getRepository(User::class);
        $user = $repo->findOneBy(array("nom"=>$nom));
        
        if ($user) $data = array("success" => 0);
        else {
            // On créé un utilisateur
            $user = new User();
            $user->setNom($nom);
            $user->setPicture($img);
            $encoded = $encoder->hashPassword($user, $password);
            $user->setPassword($encoded);
    
            // On stocke l'utilisateur
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();

            $data = array
            (
                "success" => 1,
                "id" => $user->getId(),
                "nom" => $user->getNom(),
                "img" => $user->getPicture(),
                "roles" => $user->getRoles(),
            );

            $token = new UsernamePasswordToken($user, null, 'main', $user->getRoles());
            $authenticatedToken = $this->authenticationManager->authenticate($token);
            $this->token->setToken($authenticatedToken);
        }

        // On renvoie l'utilisateur fraichement créé
        $reponse = new Response(json_encode($data));

        $reponse->headers->set("Content-Type", "application/json");
        $reponse->headers->set("Access-Control-Allow-Origin", "*");
        return $reponse;
    }

    /**
     * @Route("/edition/{id<\d+>}", name="edition_user")
     */
    public function edition(Request $request, UserPasswordHasherInterface $encoder, UserRepository $userRepository, int $id) : Response
    {
        
        if (
            !$this->token->getToken() ||
            !in_array("utilisateur", $this->token->getToken()->getUser()->getRoles()) ||
            (
                !in_array("administrateur", $this->token->getToken()->getUser()->getRoles()) &&
                $this->token->getToken()->getUser()->getId() != $id
            )
        ) return $this->redirectToRoute("app");
                
        $body = json_decode($request->getContent(), true);
        $repo = $this->getDoctrine()->getRepository(User::class);
        $user = $repo->findOneBy(array("nom"=>$body["nom"]));
        
        if ($user && $user->getNom() != $body["nom"]) $data = array("success" => 0);
        else {
            // On récupère les données
            $user = $userRepository->find($id);
            $nom = $body["nom"];
            $password = $body["pwd"];
            $roles = isset($body["roles"]) ? array($body["roles"]) : $user->getRoles();
            $img = $body["img"] ?? $user->getPicture();

            // On modifie l'utilisateur
            $user->setNom($nom);
            $user->setPicture($img);
            $encoded = $encoder->hashPassword($user, $password);
            $user->setPassword($encoded);
            $user->setRoles($roles);
    
            // On stocke l'utilisateur
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();

            $data = array
            (
                "success" => 1,
                "id" => $user->getId(),
                "nom" => $user->getNom(),
                "roles" => $user->getRoles(),
                "img" => $user->getPicture()
            );
        }
        
        // On renvoie l'utilisateur fraichement créé
        $reponse = new Response(json_encode($data));

        $reponse->headers->set("Content-Type", "application/json");
        $reponse->headers->set("Access-Control-Allow-Origin", "*");
        return $reponse;
    }

    /**
     * @Route("/suppression/{id}", name="suppression_user")
     */
    public function suppression( UserRepository $userRepository, int $id ) : Response
    {
        if (
            !$this->token->getToken() ||
            !in_array("administrateur", $this->token->getToken()->getUser()->getRoles())
        ) return $this->redirectToRoute("app");

        $user = $userRepository->find($id);

        $em = $this->getDoctrine()->getManager();
        $em->remove($user);
        $em->flush();

        $success = $userRepository->find($id) == null ? 1 : 0;
        
        $reponse = new Response(json_encode(array("success" => $success)));
        $reponse->headers->set("Content-Type", "application/json");
        $reponse->headers->set("Access-Control-Allow-Origin", "*");
        return $reponse;
    }
}
