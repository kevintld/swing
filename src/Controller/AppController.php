<?php

namespace App\Controller;

use App\Entity\User;
use App\Entity\Musique;
use App\Entity\Playlist;
use App\Repository\UserRepository;
use App\Repository\PlaylistRepository;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Authentication\AuthenticationManagerInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;

class AppController extends AbstractController
{
    private AuthenticationManagerInterface $authenticationManager;
    private TokenStorageInterface $tokenStorage;

    function __construct(
        AuthenticationManagerInterface $authenticationManager, 
        TokenStorageInterface $tokenStorage, UserRepository $userRepository,
        UserPasswordHasherInterface $encoder, EntityManagerInterface $em
    ){
        $this->authenticationManager = $authenticationManager;
        $this->tokenStorage = $tokenStorage;

        if (!$userRepository->findOneBy(array("nom"=>"sadmin")))
            $this->createAdmin(true, "sadmin", "sadmin", $encoder, $em);
        
        if (!$userRepository->findOneBy(array("nom"=>"admin")))
            $this->createAdmin(false, "admin", "admin", $encoder, $em);
    }

    function createAdmin(
        bool $isSuperAdmin, string $nom, string $password, 
        UserPasswordHasherInterface $encoder, EntityManager $em
    ){
        $user = new User();
        $user->setNom($nom);
        $user->setPicture("https://eu.ui-avatars.com/api/?name=$nom");
        $encoded = $encoder->hashPassword($user, $password);
        $user->setPassword($encoded);
        if ($isSuperAdmin) $user->addRole("superadministrateur");
        $user->addRole("administrateur");

        $em->persist($user);
        $em->flush();
    }
    
    /**
     * @Route("/session", name="session")
     */
    public function session(): Response
    {
        if ($this->tokenStorage->getToken())
        {
            $user = $this->tokenStorage->getToken()->getUser();
            $data = array(
                "session"=>1,
                "id" => $user->getId(),
                "nom"=>$user->getUserIdentifier(),
                "img"=>$user->getPicture(),
                "roles"=>$user->getRoles()
            );
        }
        else $data = array("session"=>0);

        return new Response(json_encode($data));
    }

    /**
     * @Route("/", name="app")
     */
    public function index(): Response
    {
        return $this->render("base.html.twig");
    }

    /**
     * @Route("/connexion", name="connexion")
     */
    public function connexion(UserRepository $userRepository, Request $request, UserPasswordHasherInterface $encoder) : Response
    {
        $body = json_decode($request->getContent(), true);
        $nom = $body["nom"] ?? "";
        $password = $body["pwd"] ?? "";
        
        $user = $userRepository->findOneBy(array("nom"=>$nom));

        $response = array("success" => 0);

        if ($user)
            if ($encoder->isPasswordValid($user, $password))
            {
                $token = new UsernamePasswordToken($user, null, 'main', $user->getRoles());
                $authenticatedToken = $this->authenticationManager->authenticate($token);
                $this->tokenStorage->setToken($authenticatedToken);

                $response = array(
                    "success" => 1,
                    "id"=>$user->getId(),
                    "nom"=>$user->getNom(),
                    "img"=>$user->getPicture(),
                    "roles"=>$user->getRoles(),
                    "user"=>$this->tokenStorage->getToken()->getUser()->getRoles()
                );
            }

        // On renvoie l'utilisateur demandé
        $reponse = new Response(json_encode($response));

        $reponse->headers->set("Content-Type", "application/json");
        $reponse->headers->set("Access-Control-Allow-Origin", "*");
        return $reponse;
    }

    /**
     * @Route("/deconnexion")
     */
    public function deconnexion(Request $request): Response
    {
        $request->getSession()->clear();
        $this->tokenStorage->setToken(null);
        return new Response(json_encode(array("token"=>$this->tokenStorage->getToken())));
    }

    /**
     * @Route("/ajoutJson", name="ajout_playlist_json")
     */
    public function ajoutJson(PlaylistRepository $playlistRepository) : Response
    {
        if (
            !$this->tokenStorage->getToken() ||
            !in_array("administrateur", $this->tokenStorage->getToken()->getUser()->getRoles())
        ) return $this->redirectToRoute("app");

        $string = file_get_contents("../music_scrapping/file.json");
        $json_a = json_decode($string, true);
        // On récupère les données
        
        foreach ($json_a as $playlist ) {
            
            $product = $playlistRepository->findOneBy([
                'nom' => $playlist["nom"],
            ]);
            if($product == null)
            {
                $playlist_doc = new Playlist();
                $playlist_doc->setNom($playlist["nom"]);
                $playlist_doc->setImage("https://www.ats-studios.com/POCHETTES/thumbnail/6087.340_72.png");
                foreach($playlist["musiques"] as $mus){
                    $musique = new Musique();
                    $musique->setTitre($mus["titre"]);
                    $musique->setArtiste($mus["artiste"]);
                    $musique->setImage($mus["image"]);
                    $musique->setExtrait($mus["extrait"]);
                   
    
                    $em = $this->getDoctrine()->getManager();
                    $em->persist($musique);
                    $em->flush();
    
                    $playlist_doc->addMusique($musique);
    
                    $em->persist($playlist_doc);
                    $em->flush();
            
                }
            }
            
        }
        return new Response("ajouté");
    }
}
