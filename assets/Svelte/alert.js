class CustomBSAlert {
    constructor(){}

    alert(type, msg)
    {
        let div = document.createElement("div");
        div.classList.add("alert", `alert-${type}`, "alert-dismissible", "fade", "show");
        div.setAttribute("role","alert");

        let message = document.createElement("strong");
        message.innerText = `${msg}`;

        let button = document.createElement("button");
        button.classList.add("btn-close");
        button.setAttribute("type","button");
        button.setAttribute("data-bs-dismiss","alert");
        button.setAttribute("aria-label","close");

        div.appendChild(message);
        div.appendChild(button);

        document.body.insertBefore(div, document.getElementById("container"));

        setTimeout(()=>{div.remove()},4000);
    }
}

module.exports.CustomBSAlert = CustomBSAlert;