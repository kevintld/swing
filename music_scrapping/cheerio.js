const cheerio = require('cheerio');
const cheerioChild = require('cheerio')
const got = require('got');
const fs = require('fs')
 
const vgmUrl= 'https://www.ats-studios.com/bibliotheque-musicale/';
 
class Musique
{
    constructor(titre, image, artiste, extrait) {
        this.titre =titre;
        this.image =image;
        this.artiste =artiste;
        this.extrait =extrait;      
    }
    toString()
    {
        console.log("*--------------------------------------------------------------------*")
        console.log( "titre : "  + this.titre);
        console.log( "image : "  + this.image);
        console.log( "auteur(s) : "  + this.artiste);
        console.log( "extrait : " + this.extrait);
        console.log("*--------------------------------------------------------------------*\n\n")
    }
}
class Playlist{
    constructor(titre,music_list)
    {
        this.nom = titre;
        this.musiques = music_list;
    }
}

(async () => {
    const response = await got(vgmUrl);
    const $ = cheerio.load(response.body);
    const playlists = new Array();
    fs.appendFile('file.json', '[', err => {
        if (err) {
            console.error(err)
            return
        }
        //done!
        })
    
    $('div.bloc-list-playlist a').each(function(){
        getMusique(vgmUrl + $(this).attr("href"),$(this).text())
    });
    
})();

async function getMusique(link,titre) {
    const responseChildPage = await got(link);
    const child = cheerioChild.load(responseChildPage.body);
 
    var musiques = new Array();
    child('div.bm-produit').each(function(){
        var titre = child(this).find(".bm-produit-titre").text();
        var image = child(this).attr("style").replace("background-image:url('../..","https://www.ats-studios.com").replace("');","");
        var artiste =child(this).find(".bm-produit-auteur").text();
        var extrait ="https://www.ats-studios.com" + child(this).find(".img_player").data("src");
        
        musiques.push(new Musique(titre, image, artiste, extrait));
    });    

    let playlist = new Playlist(titre,musiques);

    fs.appendFile('file.json', JSON.stringify(playlist)+',', err => {
    if (err) {
        console.error(err)
        return
    }
    //done!
    })

}

