#!/bin/bash

dockerize -wait tcp://localhost:3306 -timeout 1m


cd /var/www
chmod 777 /var/*.db
rm -r migrations/*
symfony console cache:clear
symfony console doctrine:database:create --if-not-exists
symfony console doctrine:migrations:diff --no-interaction
symfony console doctrine:migrations:migrate --no-interaction
node ./music_scrapping/cheerio.js
apache2-foreground